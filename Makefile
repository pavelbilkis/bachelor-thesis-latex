# settings precedence: command line>usercfg.mk>{windows,unix}.mk

# user settings
# include before variable definitions
ifneq ($(wildcard usercfg.mk),)
	include usercfg.mk
endif

# platform specific settings
# include before variable definitions
ifeq ($(OS),Windows_NT)
	include windows.mk
else
	include unix.mk
endif

# Ghostscript-based pdf postprocessing
include compress.mk

# Config file
MKRC ?= latexmkrc

# Source .tex file
SOURCE ?= bachelor-thesis pre-thesis-practice

# LaTeX compiler output .pdf file
TARGET ?= $(SOURCE)

# LaTeX version:
# -pdf		= pdflatex
# -pdfdvi	= pdflatex with dvi
# -pdfps	= pdflatex with ps
# -pdfxe	= xelatex with dvi (faster than -xelatex)
# -xelatex	= xelatex without dvi
# -pdflua	= lualatex with dvi  (faster than -lualatex)
# -lualatex	= lualatex without dvi
BACKEND ?= -pdfxe

# Do not modify the section below. Edit usercfg.mk instead.
DRAFTON ?= # 1=on;0=off
FONTFAMILY ?= # 0=CMU;1=MS fonts;2=Liberation fonts
ALTFONT ?= # 0=Computer Modern;1=pscyr;2=XCharter
USEBIBER ?= # 0=bibtex8;1=biber
IMGCOMPILE ?= # 1=on;0=off
NOTESON ?= # 0=off;1=on, separate slide;2=on, same slide
LATEXFLAGS ?= -halt-on-error -file-line-error
LATEXMKFLAGS ?= -silent
BIBERFLAGS ?= # --fixinits
REGEXDIRS ?= . include config include Synopsis slides # distclean dirs
TIMERON ?= # show CPU usage

# Makefile options
MAKEFLAGS := -s
.DEFAULT_GOAL := all
.NOTPARALLEL:

export DRAFTON
export FONTFAMILY
export ALTFONT
export USEBIBER
export IMGCOMPILE
export NOTESON
export LATEXFLAGS
export BIBERFLAGS
export REGEXDIRS
export TIMERON

##! компиляция всех файлов
all: bachelor-thesis pre-thesis-practice presentation

define compile
	latexmk -norc -r $(MKRC) $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) $(SOURCE)
endef

##! компиляция диссертации
bachelor-thesis: TARGET=bachelor-thesis
bachelor-thesis: SOURCE=bachelor-thesis
bachelor-thesis:
	$(compile)

##! компиляция отчёта по практике
pre-thesis-practice: TARGET=pre-thesis-practice
pre-thesis-practice: SOURCE=pre-thesis-practice
pre-thesis-practice:
	$(compile)

##! компиляция автореферата
synopsis: TARGET=synopsis
synopsis: SOURCE=synopsis
synopsis:
	$(compile)

##! компиляция презентации
speech: TARGET=slides/speech
speech: SOURCE=slides/speech
speech:
	$(compile)


presentation: speech
presentation: TARGET=presentation
presentation: SOURCE=presentation
presentation:
	$(compile)

##! компиляция черновика диссертации
bachelor-thesis-draft: DRAFTON=1
bachelor-thesis-draft: bachelor-thesis

##! компиляция черновика автореферата
synopsis-draft: DRAFTON=1
synopsis-draft: synopsis

##! компиляция диссертации, автореферата, и презентации при помощи pdflatex
pdflatex: BACKEND=-pdf
pdflatex: bachelor-thesis pre-thesis-practice synopsis presentation

##! компиляция черновиков всех файлов
draft: bachelor-thesis-draft synopsis-draft

##! компиляция автореферата в формате А4 для печати
synopsis-booklet: synopsis
synopsis-booklet: SOURCE=synopsis_booklet
synopsis-booklet: TARGET=synopsis_booklet
synopsis-booklet:
	$(compile)

##! добавление .pdf автореферата и диссертации в систему контроля версий
release: all
	git add bachelor-thesis.pdf
	git add presentation.pdf

##! очистка от временных файлов цели TARGET
clean-target:
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) -c $(SOURCE)

##! полная очистка от временных файлов цели TARGET
distclean-target:
	latexmk -norc -r $(MKRC) -f $(LATEXMKFLAGS) $(BACKEND) -jobname=$(TARGET) -C $(SOURCE)

##! очистка проекта от временных файлов
clean:
	"$(MAKE)" SOURCE=bachelor-thesis TARGET=bachelor-thesis clean-target
	"$(MAKE)" SOURCE=synopsis TARGET=synopsis clean-target
	"$(MAKE)" SOURCE=presentation TARGET=presentation clean-target

##! полная очистка проекта от временных файлов
distclean:
	"$(MAKE)" SOURCE=bachelor-thesis TARGET=bachelor-thesis distclean-target
	"$(MAKE)" SOURCE=synopsis TARGET=synopsis distclean-target
	"$(MAKE)" SOURCE=presentation TARGET=presentation distclean-target

# include after "all" rule
include examples.mk

.PHONY: all bachelor-thesis synopsis presentation bachelor-thesis-draft \
synopsis-draft pdflatex draft synopsis-booklet release clean-target \
distclean-target clean distclean
